# Medusa Push Button

![Medusa Push Button](https://shop.edwinrobotics.com/3971-thickbox_default/medusa-push-button.jpg "Medusa Push Button")

[Medusa Push Button](https://shop.edwinrobotics.com/new-products/1183-medusa-push-button.html)

The Medusa Push button comes with  a 7mm momentary switch with on board pullup for easy interfacing with microcontrollers.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.